﻿using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using NOlx.Models;
using NOlx.Models.DTOModels;
using NOlx.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.Web.Script.Serialization;

namespace NOlx.Controllers
{
    public class HouseController : Controller
    {
        private ApplicationDbContext db = ApplicationDbContext.Create();
        private readonly IHouseService _houseService;
        public HouseController()
        {
            _houseService = new HouseService();
        }
        private string defaultImagePath = "../Content/Images/Default.png";
        // GET: House
        public ActionResult Index(SearchFilter searchFilter)
        {
            var  houses = db.Houses.Include("User");
            if (TempData.ContainsKey("message"))
            {
                ViewBag.message = TempData["message"].ToString();
            }
            ViewBag.Houses = _houseService.GetHousesByFilters(searchFilter);
            ViewBag.SearchFilters = searchFilter;
            return View();
        }

        public void GetHouses(SearchFilter searchFilter)
        {
            ViewBag.Houses = _houseService.GetHousesByFilters(searchFilter);
        }

        public ActionResult Show(int id) 
        {
            House house = db.Houses.Find(id);
            if (house == null)
            {
                return HttpNotFound();
            }
            return View(house);
        }

        public ActionResult MyHouses()
        {
            // daca nu este autentificat
            if (!User.Identity.IsAuthenticated)
            {   
                // redirect catre pagina de locare
                return RedirectToAction("Login", "Account");
            }

            var UserId = User.Identity.GetUserId();

            var houses = db.Houses.Include("User").Where(x => x.UserId == UserId);
            if (TempData.ContainsKey("message"))
            {
                ViewBag.message = TempData["message"].ToString();
            }

            // var houses2 = from house in houses select house;
            ViewBag.Houses = houses;

            return View();
        }

        public ActionResult New()
        {
            // daca nu este autentificat
            if (!User.Identity.IsAuthenticated)
            {   
                // redirect catre pagina de locare
                return RedirectToAction("Login", "Account");
            }
            else 
            {
                House house = new House();
                house.UserId = User.Identity.GetUserId();
                return View(house);
            }
        }
     

        [HttpPost]
        public ActionResult New(House house, HttpPostedFileBase image1)
        {
            try
            {
                if (ModelState.IsValid)
                {   
                    if(image1 !=null)
                    {
                        house.Image = new byte[image1.ContentLength];
                        image1.InputStream.Read(house.Image, 0, image1.ContentLength);    
                    }
                    else 
                    {
                        house.Image = System.IO.File.ReadAllBytes(System.Web.HttpContext.Current.Server.MapPath(defaultImagePath));
                    }
                    
                    db.Houses.Add(house);
                    db.SaveChanges();
                    TempData["message"] = TempData["message"] + "Anuntul a fost adaugat!";
                    return RedirectToAction("Index");
                }
                else
                {
                    return View(house);
                }
            }
            catch (Exception e)
            {
                return View(house);
            }
        }
       
        public ActionResult Edit(int id)
        {
            House house = db.Houses.Find(id);
            ViewBag.House = house;
            if (house.UserId == User.Identity.GetUserId() || User.IsInRole("Administrator"))
            {
                return View(house);
            }
            else
            {
                TempData["message"] = "Nu aveti dreptul sa faceti modificari asupra unui anunt care nu va apartine!";
                return RedirectToAction("Index");
            }
        }

        [HttpPut]
        public ActionResult Edit(int id, House requestHouse)
        {
            try
            {
                House house= db.Houses.Find(id);
                    if (ModelState.IsValid)
                    {
                        if (TryUpdateModel(house))
                           {
                     
                            house.Title = requestHouse.Title;
                            house.Description = requestHouse.Description;
                            house.Address = requestHouse.Address;
                            house.OverallQual = requestHouse.OverallQual;
                            house.Price = requestHouse.Price;
                            house.SuggestedPrice = requestHouse.SuggestedPrice;
                            house.SuggestedPrice = requestHouse.SuggestedPrice;
                            house.MSSubClass = requestHouse.MSSubClass;
                            house.FullBath = requestHouse.FullBath;
                            house.TotRmsAbvGrd = requestHouse.TotRmsAbvGrd;
                            house.YearBuilt = requestHouse.YearBuilt;
                            house.GrLivArea = requestHouse.GrLivArea;
                            house.KitchenQual = requestHouse.KitchenQual;
                            house.GarageCars = requestHouse.GarageCars;
                            db.SaveChanges();     
                        }
                    }
                    else
                    {
                    return View(house);
                    }
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View();
            }
        }

        [HttpDelete]
        public ActionResult Delete(int id)
        {
            House house = db.Houses.Find(id);
            if (house.UserId == User.Identity.GetUserId() || User.IsInRole("Administrator"))
            {
                db.Houses.Remove(house);
                db.SaveChanges();
            }
            else
            {
                TempData["message"] = "Nu aveti dreptul sa stergeti un anunt care nu va apartine!";
                return RedirectToAction("Index");
            }
            return RedirectToAction("MyHouses");
        }

     

        
        // nu o sa mearga pe calculatorul vostru ( cai absolute si dependinte de pachete python )
        // [HttpPost]
        // public JsonResult GetPredictedPrice(House house)
        // {
        //     string result;
        //     string errors;
        //     string scriptPath = "C:\\PythonWork\\NOlx\\Web_Module\\NOlx\\Controllers\\predict.py";
        //     string arguments = house.OverallQual.ToString() + ' ' + house.MSSubClass.ToString() + ' ' + house.FullBath.ToString() + ' ' 
        //                      + house.TotRmsAbvGrd.ToString() + ' ' + house.YearBuilt.ToString() + ' ' + house.GrLivArea.ToString() + ' ' 
        //                      + house.KitchenQual.ToString() + ' ' + house.GarageCars.ToString();

        //     ProcessStartInfo start = new ProcessStartInfo();
        //     start.FileName = "C:\\Users\\georg\\Anaconda3\\envs\\tensorflow_env\\python.exe";
        //     // var json = new JavaScriptSerializer().Serialize(house);
        //     start.Arguments = scriptPath + " " + arguments;
        //     start.UseShellExecute = false;
        //     start.RedirectStandardOutput = true;
        //     start.CreateNoWindow = true;
        //     start.RedirectStandardError = true;
        //     using (Process process = Process.Start(start))
        //     {
        //         using (StreamReader reader = process.StandardOutput)
        //         {
        //             errors = process.StandardError.ReadToEnd();
        //             result = reader.ReadToEnd();
        //             Console.Write(errors);
        //             Console.Write(result);
        //         }
        //     }
        //     return Json(result);   
        // }

        // functie 'placeholder'
        [HttpPost]
        public JsonResult GetPredictedPrice(House house)
        {
            Random rnd = new Random();
            return Json(rnd.Next(10000, 1500000));
        }

    }
    public static class Utilss
    {
        private static ApplicationDbContext _context = ApplicationDbContext.Create();
        public static string GetUserName(string id)
        {
            return _context.Users.FirstOrDefault(x => x.Id == id).UserName.ToString();
        }
        public static string GetMsSubClass(int id)
        {
            var msSubClasses = new List<SelectListItem>
            {
                new SelectListItem { Text = "Selectati...", Value = "-1" },
                new SelectListItem { Text = "1 etaj - an 1946 sau mai nou", Value = "5" },
                new SelectListItem { Text = "1 etaj - an 1945 sau mai vechi", Value = "6" },
                new SelectListItem { Text = "1 etaj - mansarda finisata - orice an", Value = "7" },
                new SelectListItem { Text = "1-1/2 etaje - nefinisat - orice an", Value = "8" },
                new SelectListItem { Text = "1-1/2 etaje - finisat - orice an", Value = "9" },
                new SelectListItem { Text = "2 etaje - an 1946 sau mai nou", Value = "10" },
                new SelectListItem { Text = "2 etaje - an 1945 sau mai vechi", Value = "11" },
                new SelectListItem { Text = "2-1/2 etaje - orice an", Value = "12" },
                new SelectListItem { Text = "MULTILEVEL", Value = "13" },
                new SelectListItem { Text = "Foaier impartit", Value = "15" },
                new SelectListItem { Text = "Duplex - orice an", Value = "16" },
                new SelectListItem { Text = "1 etaj PUD - an 1946 sau mai nou", Value = "0" },
                new SelectListItem { Text = "1-1/2 etaje PUD - orice varsta", Value = "1" },
                new SelectListItem { Text = "2 etaje PUD - 1946 sau mai nou", Value = "2" },
                new SelectListItem { Text = "PUD - MULTILEVEL", Value = "3" },
                new SelectListItem { Text = "Conversie 2 familii - orice an", Value = "4" }
            };

            return msSubClasses.FirstOrDefault(x => x.Value == id.ToString())?.Text;
        }
        public static string GetOverallQual(int id)
        {
            var overallQuals = new List<SelectListItem>
                         {
                         new SelectListItem{ Text="Foarte Excelent",   Value = "10" },
                         new SelectListItem{ Text="Excelent",          Value = "9" },
                         new SelectListItem{ Text="Foarte Bun",        Value = "8" },
                         new SelectListItem{ Text="Bun",               Value = "7" },
                         new SelectListItem{ Text="Peste medie",        Value = "6" },
                         new SelectListItem{ Text="Medie",              Value = "5" },
                         new SelectListItem{ Text="Sub medie",          Value = "4" },
                         new SelectListItem{ Text="Rezonabil",         Value = "3" },
                         new SelectListItem{ Text="Prost",            Value = "2" },
                         new SelectListItem{ Text="Foarte Prost",     Value = "1" },
                         };

            return overallQuals.FirstOrDefault(x => x.Value == id.ToString())?.Text;
        }
        public static string KitchenQual(int id)
        {
            var kitchenQuals = new List<SelectListItem>
            {
                new SelectListItem{ Text="Selectati...", Value = "-1", Selected = true },
                new SelectListItem{ Text="Excelent", Value = "4" },
                new SelectListItem{ Text="Bun", Value = "3" },
                new SelectListItem{ Text="Medie", Value = "2" },
                new SelectListItem{ Text="Rezonabil", Value = "1" },
                new SelectListItem{ Text="Prost", Value = "0" }
            };

            return kitchenQuals.FirstOrDefault(x => x.Value == id.ToString())?.Text;
        }
    }
}