import sys
import pandas as pd
import pickle

if __name__ == '__main__':
	rawInput = sys.argv[1:]
	columns =  ['OverallQual', 'MSSubClass', 'FullBath', 'TotRmsAbvGrd', 'YearBuilt', 'GrLivArea', 'KitchenQual', 'GarageCars']
	temp_dic  = { value: [ rawInput[key] ] for key, value in enumerate(columns) }
	df = pd.DataFrame(data=temp_dic) 


	# filename = '../../../House_Pricing_Module/finalized_model.sav'
	filename = 'C:/PythonWork/NOlx/House_Pricing_Module/finalized_model.sav'
	model = pickle.load(open(filename, 'rb'))
	result = model.predict(df)

	print("%d" % result)

	# filename = 'House_Pricing_Module/finalized_model.sav'
	# temp_dic = {'OverallQual':[6], 'MSSubClass':[2], 'FullBath':[2], 'TotRmsAbvGrd':[6], 'YearBuilt':[1974], 
				# 'GrLivArea':[1322], 'KitchenQual':[3.0], 'GarageCars':[2.0]}



