﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NOlx.Models.DTOModels
{
    public class SearchFilter
    {
        public int PriceMin { get; set; }
        public int PriceMax { get; set; }
        public string ByTitle { get; set; }
        public int GarageCarsMin { get; set; }
        public int GarageCarsMax { get; set; }
        public int YearBuiltMin { get; set; }
        public int YearBuiltMax { get; set; }
        public int GrLivMin { get; set; }
        public int GrLivMax { get; set; }
        public bool IsOfSuggestedPrice { get; set; }
    }
}