﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NOlx.Models
{
    public class House
    {
        [Key]
        public int HouseId { get; set; }


        [Required(ErrorMessage = "Acest camp este obligatoriu")]
        public string Title { get; set; }
        public string Description { get; set; }

        [Required(ErrorMessage = "Acest camp este obligatoriu")]
        public string Address { get; set; }

        [Required(ErrorMessage = "Acest camp este obligatoriu")]
        public int OverallQual  { get; set; }

        [Required(ErrorMessage = "Acest camp este obligatoriu")]
        [Range(1, Int64.MaxValue, ErrorMessage = "Acest camp este obligatoriu")]
        public float Price { get; set; }

        [Required(ErrorMessage = "Acest camp este obligatoriu")]
        [Range(1, Int64.MaxValue, ErrorMessage = "Acest camp este obligatoriu")]
        
        public float SuggestedPrice { get; set; }
        [Required(ErrorMessage = "Acest camp este obligatoriu")]
        public int MSSubClass { get; set; }
        [Required(ErrorMessage = "Acest camp este obligatoriu")]
        [Range(0, Int64.MaxValue, ErrorMessage = "Acest camp este obligatoriu")]
        public int FullBath { get; set; }
        [Required(ErrorMessage = "Acest camp este obligatoriu")]
        [Range(1, Int64.MaxValue, ErrorMessage = "Acest camp este obligatoriu")]
        public int TotRmsAbvGrd { get; set; }

        [Required(ErrorMessage = "Acest camp este obligatoriu")]
        [Range(1, Int64.MaxValue, ErrorMessage = "Acest camp este obligatoriu")]
        public int YearBuilt { get; set; }

        [Required(ErrorMessage = "Acest camp este obligatoriu")]
        [Range(1, Int64.MaxValue, ErrorMessage = "Acest camp este obligatoriu")]
        public float GrLivArea { get; set; }
        [Required(ErrorMessage = "Acest camp este obligatoriu")]
        [Range(0, Int64.MaxValue, ErrorMessage = "Acest camp este obligatoriu")]
        public int KitchenQual { get; set; }
        public byte[] Image { get; set; }
        [Required(ErrorMessage = "Acest camp este obligatoriu")]
        public int GarageCars { get; set; }
        public string UserId { get; set; }
        public virtual ApplicationUser User { get; set; }

    }


}