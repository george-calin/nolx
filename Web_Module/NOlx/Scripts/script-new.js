var btnConfirmation;
var btnEdit;

$(document).ready(function () {
	handleInitPlaceholders();
	handleConfirmationBtn();
	handleEditBtn();
});

handleInitPlaceholders = function () { 
	$('#KitchenQual').val(-1);
	$('#MSSubClass').val(-1);
}

var handleConfirmationBtn = function() {
	btnConfirmation = $('#btn-confirmation');

	btnConfirmation.on('click', function () {
	var House = {
		OverallQual : $('#OverallQual').val(), 
		MSSubClass : $('#MSSubClass').val(),
		FullBath : $('#FullBath').val(),
		TotRmsAbvGrd : $('#TotRmsAbvGrd').val(),
		YearBuilt : $('#YearBuilt').val(),
		GrLivArea : $('#GrLivArea').val(),
		KitchenQual : $('#KitchenQual').val(),
		GarageCars : $('#GarageCars').val()
	}
		$.ajax({
			type: "POST",
			url: '/House/GetPredictedPrice',
			data: House , 
			dataType: 'json',
			success: successFunction,
			error: errorFunction
		})
	});
	
}

var successFunction = function (data) {
	$('#Price').val(data);
	$('#SuggestedPrice').val(data);

	btnConfirmation.addClass('hide');
	btnEdit.removeClass('hide');

	$('.phase-1').prop('readonly', true).addClass("myDisabled");
	$('.phase-2').prop('readonly', false);

	$('#btn-add').prop('disabled', false);
}

var errorFunction = function (data) {
	alert("Eroare Server");
}

var handleEditBtn = function () {
	btnEdit = $('#btn-edit');

	btnEdit.on('click', function() {
		btnConfirmation.removeClass('hide');
		btnEdit.addClass('hide');
	
	
		$('.phase-1').prop('readonly', false).removeClass("myDisabled");
		$('.phase-2').prop('readonly', true);

		$('#btn-add').prop('disabled', true);
	});
}
function newFunction() {
	$('.phase-1').prop('readonly', true).addClass("myDisabled");
}

