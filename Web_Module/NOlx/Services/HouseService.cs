﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NOlx.Models;
using NOlx.Models.DTOModels;

namespace NOlx.Services
{
    public class HouseService : IHouseService
    {

        private readonly ApplicationDbContext _context;

        public HouseService()
        {
            _context = ApplicationDbContext.Create();
        }

        public List<House> GetHousesByFilters(SearchFilter searchFilter)
        {
            var result = _context.Houses.AsQueryable();

            if(searchFilter.YearBuiltMax > 0)
            {
                result = result.Where(c => (c.YearBuilt <= searchFilter.YearBuiltMax));
            }

            if(searchFilter.YearBuiltMin > 0)
            {
                result = result.Where(c => c.YearBuilt > searchFilter.YearBuiltMin);
            }
            if(searchFilter.GrLivMax > 0)
            {
                result = result.Where(c => (c.GrLivArea <= searchFilter.GrLivMax));
            }

            if(searchFilter.GrLivMin > 0)
            {
                result = result.Where(c => c.GrLivArea > searchFilter.GrLivMin);
            }

            if (!string.IsNullOrEmpty(searchFilter.ByTitle))
            {
                result = result.Where(c => c.Title.Contains(searchFilter.ByTitle));
            }

            if(searchFilter.PriceMax!=0)
            {
                result = result.Where(c => c.Price <= searchFilter.PriceMax);
            }
            if(searchFilter.PriceMin!=0)
            {
                result = result.Where(c => c.Price >= searchFilter.PriceMin);
            }



            if(searchFilter.GarageCarsMax != 0)
            {
                result = result.Where(c => c.GarageCars <= searchFilter.GarageCarsMax);
            }

            if(searchFilter.GarageCarsMin != 0)
            {
                result = result.Where(c => c.GarageCars >= searchFilter.GarageCarsMin);
            }
            



            return result.ToList();
        }
    }
}