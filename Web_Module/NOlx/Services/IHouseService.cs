﻿using NOlx.Models;
using NOlx.Models.DTOModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NOlx.Services
{
    public interface IHouseService
    {
        List<House> GetHousesByFilters(SearchFilter searchFilter);
    }
}